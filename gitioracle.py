#!/usr/bin/env python

# ********************************************************************************************
#   ________.______________.___________ __________    _____  _________ .____     ___________
#  /  _____/|   \__    ___/|   \_____  \\______   \  /  _  \ \_   ___ \|    |    \_   _____/
# /   \  ___|   | |    |   |   |/   |   \|       _/ /  /_\  \/    \  \/|    |     |    __)_
# \    \_\  \   | |    |   |   /    |    \    |   \/    |    \     \___|    |___  |        \
#  \______  /___| |____|   |___\_______  /____|_  /\____|__  /\______  /_______ \/_______  /
#         \/                           \/       \/         \/        \/        \/        \/
# ********************************************************************************************
# GitiOracle's MP3 JOINER:
#
# A standalone script to join all .mp3 files, or files that have a suitable extension,
# that are in current working directory.
#
# NOTE:
# 	* Files will be joined into a single mp3 file
# 	* Files will be joined in alphabetical order
# 	* file is saved as 'output.mp3' into the same folder
#
# Author..: Nicholas E. Hamilton, PhD
# Email...: nicholasehamilton@gmail.com
# Date....: 20th July 2020

from pydub import AudioSegment
import os

# The valid extensions
VALID_EXTENSIONS = ['mp3', 'mp4']

# The Output Filename
OUTPUT_FILENAME = 'output.mp3'


def format_duration(millis):
	s = int((millis / 1000) % 60)
	m = int((millis / (1000 * 60)) % 60)
	h = int((millis / (1000 * 60 * 60)) % 24)
	return "{0}h:{1}m:{2}s".format(h, m, s)


def is_audio_file(file_name):
	"""
	Check if file name has correct extension
	:param file_name: the file name to check
	:return: true if it has one of the valid extensions, else false
	"""
	return any([file_name.lower().endswith(x) for x in VALID_EXTENSIONS])


def is_output_file(file_name):
	"""
	Check if file is the output file
	:param file_name: the file name to check
	:return: true if it is the output file, else false
	"""
	return file_name.endswith(OUTPUT_FILENAME)


def get_files():
	"""
	List all mp3 files in current directory, which do not match the output filename
	:return: array of file names
	"""
	# All files in directory
	file_array = os.listdir('./')

	# Filter
	file_array = [f for f in file_array if is_audio_file(f) and not is_output_file(f)]

	# Sort
	file_array.sort(reverse=False)

	# Done
	return file_array


def stitch_files():
	"""
	Stitch together all mp3 files in current directory
	:return: True if success, else False
	"""
	# Get the files
	files = get_files()

	# If no files detected, exit
	if not len(files):
		print("No valid files Detected, Exiting")
		return False

	print("Stitching {0}x Files: [{1}]".format(len(files), ", ".join(files)))

	# Start with an empty audio track
	output = AudioSegment.empty()

	# Now Process
	for idx, file_name in enumerate(files):
		print("{0}/{1} Processing: {2}".format(idx + 1, len(files), file_name))
		audio = AudioSegment.from_file(file_name, 'mp3')
		output += audio

	# Report Duration
	print("Combined duration is {1}".format(OUTPUT_FILENAME, format_duration(len(output))))

	# Save File
	print("Exporting to: {0}".format(OUTPUT_FILENAME))
	output.export(OUTPUT_FILENAME, format="mp3")

	return True


# Main Method
if __name__ == '__main__':
	stitch_files()


